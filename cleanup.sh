#!/usr/bin/env bash
# Author: Chmouel Boudjnah <chmouel@chmouel.com>
set -xfo pipefail

(
  git fetch origin
  git branch -r | grep origin | grep -v /main | sed "s/  origin\///" | grep pac-e2e-test
) | xargs -r -n5 git push --delete origin

(
  git branch -l | grep -v main | sed "s/^ //" | grep pac-e2e-test
) | xargs -r -n5 git push --delete origin
